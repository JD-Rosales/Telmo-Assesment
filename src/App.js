import './App.css';
import Home from './components/Home/Home';
import TourDates from './components/TourDates/TourDates';
import TheBand from './components/TheBand/TheBand';
import OurStory from './components/OurStory/OurStory';
import OurMusic from './components/OurMusic/OurMusic';
import ContactUs from './components/ContactUs/ContactUs';
import Footer from './components/Footer/Footer';
import Navbar from './components/Navbar/Navbar';

function App() {
  return (
    <div className="App">
      <Navbar />
      <Home />
      <TourDates />
      <TheBand />
      <OurStory />
      <OurMusic />
      <ContactUs />
      <Footer />
    </div>
  );
}

export default App;
