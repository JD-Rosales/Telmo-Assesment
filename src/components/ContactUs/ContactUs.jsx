import "./ContactUs.css";

const ContactUs = () => {
  return (
    <section id="contact-us">
      <div className="contact-us container">
        <header>
          <h1 className="section-title">CONTACT US</h1>
        </header>

        <h2>DROP US A MESSAGE!</h2>

        <form action="">
          <input type="text" placeholder="Full name" />
          <input type="text" placeholder="E-mail" />
          <input type="text" placeholder="Phone number" />
          <textarea
            name="message"
            id="message"
            cols="30"
            rows="10"
            placeholder="Your message"
          ></textarea>
          <button type="submit">SUBMIT</button>
        </form>

        <div className="bottom-text">
          <span>MANANGEMENT AND BOOKING</span>
          <span>Infomanagement@sample.com</span>
          <span>123 456 78922</span>
        </div>
      </div>
    </section>
  );
};

export default ContactUs;
