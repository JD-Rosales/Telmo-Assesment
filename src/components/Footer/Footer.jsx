import "./Footer.css";
import facebook from "../../assets/facebook.svg";
import instagram from "../../assets/instagram.svg";
import twitter from "../../assets/twitter.svg";
import youtube from "../../assets/youtube.svg";

const Footer = () => {
  return (
    <footer>
      <span>FOLLOW US ON:</span>

      <div className="icons-container">
        <img src={facebook} alt="Facebook" />
        <img src={twitter} alt="Twitter" />
        <img src={instagram} alt="Instagram" />
        <img src={youtube} alt="YouTube" />
      </div>
    </footer>
  );
};

export default Footer;
