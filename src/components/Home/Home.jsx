import "./Home.css";

const Home = () => {
  return (
    <section id="home">
      <div className="home container">
        <h1>TELMO BAND</h1>
      </div>
    </section>
  );
};

export default Home;
