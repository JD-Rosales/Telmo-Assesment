import "./Navbar.css";
import band_logo from "../../assets/band_logo.png";
import facebook from "../../assets/facebook.svg";
import instagram from "../../assets/instagram.svg";
import twitter from "../../assets/twitter.svg";
import youtube from "../../assets/youtube.svg";
import { useState } from "react";

const Navbar = () => {
  const [activeLink, setActiveLink] = useState("home");
  const [isActive, setIsActive] = useState(false);

  const updateActiveLink = (value) => {
    setActiveLink(value);
    setIsActive(false);
  };

  const menuClicked = () => {
    setIsActive(!isActive);
  };

  return (
    <div id="nav-container">
      <div className="nav-container container">
        <div className="nav-bar">
          <div className="brand">
            <img src={band_logo} alt="Logo" />
          </div>

          <div className="nav-list">
            <div
              className={isActive ? "active hamburger" : "hamburger"}
              onClick={menuClicked}
            >
              <div className="bar"></div>
            </div>

            <ul className={isActive ? "active" : ""}>
              <li
                onClick={() => updateActiveLink("home")}
                className={
                  activeLink === "home" ? "active navbar-link" : "navbar-link"
                }
              >
                <a href="#home">HOME</a>
              </li>
              <li
                onClick={() => updateActiveLink("tour-dates")}
                className={
                  activeLink === "tour-dates"
                    ? "active navbar-link"
                    : "navbar-link"
                }
              >
                <a href="#tour-dates">TOUR DATES</a>
              </li>
              <li
                onClick={() => updateActiveLink("the-band")}
                className={
                  activeLink === "the-band"
                    ? "active navbar-link"
                    : "navbar-link"
                }
              >
                <a href="#the-band">THE BAND</a>
              </li>
              <li
                onClick={() => updateActiveLink("our-music")}
                className={
                  activeLink === "our-music"
                    ? "active navbar-link"
                    : "navbar-link"
                }
              >
                <a href="#our-music">OUR MUSIC</a>
              </li>
              <li
                onClick={() => updateActiveLink("contact-us")}
                className={
                  activeLink === "contact-us"
                    ? "active navbar-link"
                    : "navbar-link"
                }
              >
                <a href="#contact-us">CONTACT US</a>
              </li>
            </ul>

            <div className="social-icons">
              <img src={facebook} alt="Facebook" />
              <img src={twitter} alt="Twitter" />
              <img src={instagram} alt="Instagram" />
              <img src={youtube} alt="Youtube" />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Navbar;
