import "./OurMusic.css";
import music from "../../assets/music.png";
import heart from "../../assets/heart.png";
import menu from "../../assets/menu.png";
import album_cover from "../../assets/album_cover.jpg";

const OurMusic = () => {
  return (
    <section id="our-music">
      <div className="our-music container">
        <header>
          <h1 className="section-title">OUR MUSIC</h1>
        </header>

        <main>
          <div className="top-songs">
            <h2>TOP SONGS</h2>
            <div className="song-container">
              <div>
                <img src={music} alt="Music" />
                <img className="favorite" src={heart} alt="Favorite" />
              </div>

              <span className="title">SONG TITLE</span>

              <div className="option">
                <span className="duration">3:41</span>
                <img src={menu} alt="Menu" />
              </div>
            </div>

            <div className="song-container">
              <div>
                <img src={music} alt="Music" />
                <img className="favorite" src={heart} alt="Favorite" />
              </div>

              <span className="title">SONG TITLE</span>

              <div className="option">
                <span className="duration">3:41</span>
                <img src={menu} alt="Menu" />
              </div>
            </div>

            <div className="song-container">
              <div>
                <img src={music} alt="Music" />
                <img className="favorite" src={heart} alt="Favorite" />
              </div>

              <span className="title">SONG TITLE</span>

              <div className="option">
                <span className="duration">3:41</span>
                <img src={menu} alt="Menu" />
              </div>
            </div>

            <div className="song-container">
              <div>
                <img src={music} alt="Music" />
                <img className="favorite" src={heart} alt="Favorite" />
              </div>

              <span className="title">SONG TITLE</span>

              <div className="option">
                <span className="duration">3:41</span>
                <img src={menu} alt="Menu" />
              </div>
            </div>
          </div>
          <div className="albums">
            <h2>ALBUMS</h2>

            <div className="album-container">
              <div className="card">
                <div className="image-container">
                  <img src={album_cover} alt="Album Cover" />
                </div>

                <span className="album-name">Album 1</span>
                <span>Lorem ipsum</span>
              </div>

              <div className="card">
                <div className="image-container">
                  <img src={album_cover} alt="Album Cover" />
                </div>

                <span className="album-name">Album 2</span>
                <span>Lorem ipsum</span>
              </div>

              <div className="card">
                <div className="image-container">
                  <img src={album_cover} alt="Album Cover" />
                </div>

                <span className="album-name">Album 3</span>
                <span>Lorem ipsum</span>
              </div>

              <div className="card">
                <div className="image-container">
                  <img src={album_cover} alt="Album Cover" />
                </div>

                <span className="album-name">Album 4</span>
                <span>Lorem ipsum</span>
              </div>
            </div>
          </div>
        </main>
      </div>
    </section>
  );
};

export default OurMusic;
