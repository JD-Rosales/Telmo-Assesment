import "./OurStory.css";
import { OurStoryData } from "./OurStoryData";

const OurStory = () => {
  return (
    <section id="our-story">
      <div className="our-story container">
        <header>
          <h1 className="section-title">OUR STORY</h1>
        </header>

        <main>
          <div className="text-container">
            <div>
              <p className="text-header">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla
                accumsan convallis ante quis congue. lobortis sed justo. Vivamus
                vel interdum tellus, euismod rutrum nisl. Orci varius natoque
                penatibus et magnis dis parturient montes, nascetur ridiculus
                mus.Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Nulla accumsan convallis ante quis congue.
              </p>

              <br />

              <p>
                Aliquam lobortis tincidunt mattis. Nunc mauris massa, egestas
                quis tincidunt in, lobortis sed justo. Vivamus vel interdum
                tellus, euismod rutrum nisl. Orci varius natoque penatibus et
                magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum
                dolor sit amet, consectetur adipiscing elit. Nulla accumsan
                convallis ante quis congue.nascetur ridiculus mus.Lorem ipsum
                dolor sit amet, consectetur adipiscing elit.
                <span> Nulla accumsan convallis ante quis congue.</span>
              </p>
            </div>
          </div>

          <div className="right-side">
            <div className="video-container">
              <iframe
                src="https://www.youtube.com/embed/v2AC41dglnM"
                title="AC/DC - Thunderstruck (Official Video)"
                frameBorder="0"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowFullScreen
              ></iframe>
            </div>
          </div>
        </main>

        <div className="members-container">
          {OurStoryData.map((data, index) => {
            return (
              <div key={index} className="card">
                <div className="image-container">
                  <img src={data.image} alt="Band Member" />
                </div>
                <span className="name">{data.name}</span>
                <span>{data.role}</span>
              </div>
            );
          })}
        </div>
      </div>
    </section>
  );
};

export default OurStory;
