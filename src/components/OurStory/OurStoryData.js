import vocalist from "../../assets/vocalist.jpg"
import guitarist from "../../assets/guitarist.jpg"
import bassist from "../../assets/bassist.jpg"
import drummer from "../../assets/drummer.jpg"

export const OurStoryData = [
  {
    image: vocalist,
    name: "JAUN DELA CRUZ",
    role: "VOCALIST"
  },
  {
    image: guitarist,
    name: "CARDO DALISAY",
    role: "LEAD GUITAR"
  },
  {
    image: bassist,
    name: "PEDRO PENDUCO",
    role: "BASSIST"
  },
  {
    image: drummer,
    name: "RASTAMAN",
    role: "DRUMMER"
  },
]