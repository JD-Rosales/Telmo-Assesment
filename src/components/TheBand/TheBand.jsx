import "./TheBand.css";
import the_band from "../../assets/the_band.jpg";

const TheBand = () => {
  return (
    <section id="the-band">
      <div className="the-band container">
        <header>
          <h1 className="section-title">THE BAND</h1>
        </header>

        <main>
          <div className="image-container">
            <img src={the_band} alt="The Band" />
          </div>
          <div className="text-container">
            <div>
              <div className="text-header">
                <p>
                  <i>
                    “Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Nulla accumsan.’’
                  </i>
                </p>
                <p className="text">-The critic-</p>
              </div>

              <p>
                Aliquam lobortis tincidunt mattis. Nunc mauris massa, egestas
                quis tincidunt in, lobortis sed justo. Vivamus vel interdum
                tellus, euismod rutrum nisl. Orci varius natoque penatibus et
                magnis dis parturient montes, nascetur ridiculus mus.Lorem ipsum
                dolor sit amet.
              </p>
            </div>
          </div>
        </main>
      </div>
    </section>
  );
};

export default TheBand;
