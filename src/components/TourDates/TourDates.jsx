import "./TourDates.css";
import { TourDatesData } from "./TourDatesData";

const TourDates = () => {
  return (
    <section id="tour-dates">
      <div className="tour-dates container">
        <header>
          <h1 className="section-title">TOUR DATES</h1>
        </header>

        <main>
          {TourDatesData.map((data, index) => {
            return (
              <div key={index} className="tour-cards">
                <span>{data.date}</span>
                <div className="image-container">
                  <img src={data.image} alt="Location" />
                </div>
                <p>{data.desc}</p>
                <span>{data.location}</span>
              </div>
            );
          })}
        </main>
      </div>
    </section>
  );
};

export default TourDates;
