import location1 from "../../assets/location1.jpg"
import location2 from "../../assets/location2.jpg"
import location3 from "../../assets/location3.jpg"
import location4 from "../../assets/location4.jpg"

export const TourDatesData = [
  {
    date: "Saturday, September 11, 2022",
    image: location1,
    desc: "Lorem ipsum dolor sit amet.",
    location: "Brgy 1, Tacloban City Philippines"
  },
  {
    date: "Saturday, October 12, 2022",
    image: location2,
    desc: "Lorem ipsum dolor sit amet.",
    location: "Brgy 2, Tacloban City Philippines"
  },
  {
    date: "Saturday, November 13, 2022",
    image: location3,
    desc: "Lorem ipsum dolor sit amet.",
    location: "Brgy 3, Tacloban City Philippines"
  },
  {
    date: "Saturday, December 14, 2022",
    image: location4,
    desc: "Lorem ipsum dolor sit amet.",
    location: "Brgy 4, Tacloban City Philippines"
  },
]